import './Header.css'

const Header = () => {
  return (
    <div className='header'>
      <span className='header-title'>HN Feed</span>
      <span className='header-subtitle'>{`We <3 hacker news`}</span>
    </div>
  )
}

export default Header