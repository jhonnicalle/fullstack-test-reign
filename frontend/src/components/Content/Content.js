import './Content.css'
import trash from '../../images/trash.png'
import { useEffect, useState } from 'react'
import { API } from '../../utils/http-common'
import moment from 'moment'

const Content = () => {
  const [isLoading, setIsLoading] = useState(true)
  const [data, setData] = useState([])

  const getData = () => {
    API.get('hits/')
    .then(res => {
      const organizedData = res.data.sort((a,b) => {return new Date(b.created_at) - new Date(a.created_at)})
      setData(organizedData)
      setIsLoading(false)
    })
    .catch(err => {
      setIsLoading(false)
    })
    
  }

  const deleteItem = (idHit) => {
    API.delete(`hits/${idHit}`)
    .then(res => {
      setIsLoading(true);
      getData();
    })
  }

  useEffect(() => {
    getData()
  }, [])

  return (
    <div className="content">
      {isLoading ? (
        <div className="content-message">Loading...</div>
      ) : (
        <>
          {data.length === 0 ? (
            <div className="content-message">There is no data</div>
          ) : (
            <>
              {data.map((item, i) => {
                if (item.story_title !== null || item.title !== null) {
                  return (
                    <div key={i} className='content-item'>
                      <a className='content-item-url' href={item.story_url} >
                        <div className='content-item-info'>
                          <span className='content-item-info-title'>{item.story_title !== null ? item.story_title : item.title}<span className='content-item-info-author'>- {item.author} -</span></span>
                          <span className='content-item-info-time'>{moment(item.created_at).format('DD/MM/YYYY HH:MM:SS')}</span>
                        </div>
                      </a>
                      <div className='content-item-delete'>
                        <img className='content-item-delete-image' src={trash} alt='' onClick={() => deleteItem(item.id)} />
                      </div>
                    </div>
                  )
                }
              })}
            </>
          )}
        </>
      )}
    </div>
  )
}

export default Content