import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Hit } from './hits.model';

@Injectable()
export class HitsService {
  constructor(@InjectModel('Hits') private readonly hitModel: Model<Hit>) {}

  async insertHit(
    created_at: string,
    title: string,
    url: string,
    author: string,
    story_title: string,
    story_url: string,
  ) {
    const newHit = new this.hitModel({
      created_at,
      title,
      url,
      author,
      story_title,
      story_url,
    });
    const result = await newHit.save();
    return result;
  }

  async getHits() {
    const hitsList = await this.hitModel.find();

    if (!hitsList) {
      throw new NotFoundException('There is no hits');
    }

    return hitsList;
  }

  async deleteHit(hitId: string) {
    try {
      await this.hitModel.deleteOne({ _id: hitId }).exec();
      return { sucess: true };
    } catch (error) {
      throw new NotFoundException(`Hit doesn't found`);
    }
  }
}
