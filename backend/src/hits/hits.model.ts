import * as mongoose from 'mongoose';

export const HitsSchema = new mongoose.Schema(
  {
    created_at: { type: String },
    title: { type: String },
    url: { type: String },
    author: { type: String },
    story_title: { type: String },
    story_url: { type: String },
  },
  {
    _id: true,
    collection: 'collection_name',
    id: true,
    toJSON: {
      virtuals: true,
      versionKey: true,
    },
  },
);

export interface Hit {
  id: string;
  created_at: string;
  title: string;
  url: string;
  author: string;
  story_title: string;
  story_url: string;
}
