import { HttpModule, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ScheduleModule } from '@nestjs/schedule';
import { HitsController } from './hits.controller';
import { HitsSchema } from './hits.model';
import { HitsService } from './hits.service';

@Module({
  imports: [
    HttpModule,
    MongooseModule.forFeature([{ name: 'Hits', schema: HitsSchema }]),
  ],
  controllers: [HitsController],
  providers: [HitsService],
  exports: [HitsService],
})
export class HitsModule {}
