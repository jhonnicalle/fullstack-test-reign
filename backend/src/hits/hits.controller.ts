import { Body, Controller, Delete, Get, Param, Post } from '@nestjs/common';
import { HitsService } from './hits.service';

@Controller('hits')
export class HitsController {
  constructor(private readonly hitsService: HitsService) {}

  @Get()
  getAllHits(): any {
    const hitsList = this.hitsService.getHits();
    return hitsList;
  }

  @Post()
  addHit(
    @Body('created_at') hitCreatedAt: string,
    @Body('title') hitTitle: string,
    @Body('url') hitURL: string,
    @Body('author') hitAuthor: string,
    @Body('story_title') hitStoryTitle: string,
    @Body('story_url') hitStoryUrl: string,
  ): any {
    const hitRegistered = this.hitsService.insertHit(
      hitCreatedAt,
      hitTitle,
      hitURL,
      hitAuthor,
      hitStoryTitle,
      hitStoryUrl,
    );

    return hitRegistered;
  }

  @Delete(':id')
  async removeHit(@Param('id') hitId: string) {
    return await this.hitsService.deleteHit(hitId);
  }
}
