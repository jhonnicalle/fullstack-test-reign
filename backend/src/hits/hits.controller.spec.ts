import { Test, TestingModule } from '@nestjs/testing';
import { HitsController } from './hits.controller';
import { HitsService } from './hits.service';

describe('AppController', () => {
  let hitsController: HitsController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [HitsController],
      providers: [HitsService],
    }).compile();

    hitsController = app.get<HitsController>(HitsController);
  });

  describe('root', () => {
    it('should return "Hello World!"', () => {
      expect(hitsController.getAllHits());
    });
  });
});
