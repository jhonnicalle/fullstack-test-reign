import { HttpService, Injectable, Logger } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import { HitsService } from './hits/hits.service';
// import { HitsService } from './hits/hits.service';

@Injectable()
export class AppService {
  private readonly logger = new Logger();

  constructor(private http: HttpService, private hitService: HitsService) {}

  getHello(): string {
    return 'Hello World!';
  }

  @Cron('* * * *')
  insertDataEveryHour(): any {
    this.http.get('https://hn.algolia.com/api/v1/search_by_date?query=nodejs')
      .subscribe((response) => {
        response.data.hits.forEach((item) => {
          try {
            this.hitService.insertHit(item.created_at, item.title, item.url, item.author, item.story_title, item.story_url);
          } catch (err) {
            return err;
          }
        });
      });
  }
}
