import { HttpModule, Module } from '@nestjs/common';
import { ScheduleModule } from '@nestjs/schedule';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { HitsModule } from './hits/hits.module';

@Module({
  imports: [
    HttpModule,
    ScheduleModule.forRoot(),
    HitsModule,
    MongooseModule.forRoot('mongodb://mongo:27017/reign_db', { useNewUrlParser: true }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
