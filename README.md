# Fullstack Test Reign

This repository is dedicated to the Full Stack offer test by Reign company.

To build all the project, first of all you have to get installed docker and docker-compose.

Run the following command to build all the images and run them:

```bash
# For users Ubuntu/Linux
sudo docker-compose up -d --build

# For users Windows
docker-compose up -d --build
```

That command will build and run all the projects, and to see the result, you can search in your browser the next link.

> http://localhost:3000

